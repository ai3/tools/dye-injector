FROM golang:1.17.5 AS build
ADD . /src
WORKDIR /src
RUN go build -tags netgo -o /dye dye.go

FROM scratch
COPY --from=build /dye /usr/bin/dye

ENTRYPOINT ["/usr/bin/dye"]
