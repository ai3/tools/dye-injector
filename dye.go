package main

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"flag"
	"log"
	"log/syslog"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	elastic "github.com/olivere/elastic/v7"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"
)

var (
	syslogFacility    = syslog.LOG_LOCAL7
	timeSkewAllowance = 1 * time.Minute

	addr          = flag.String("addr", getenv("ADDR", ":7094"), "address to listen on")
	elasticURL    = flag.String("url", getenv("ELASTICSEARCH_URL", "http://log-collector:9200"), "Elasticsearch URL")
	indexFormat   = flag.String("index-format", "logstash-2006.01.02", "ES index format (Go time format)")
	pollInterval  = flag.Duration("poll-interval", 1*time.Second, "polling interval for ES queries")
	probeInterval = flag.Duration("interval", 1*time.Minute, "interval between e2e checks")
	probeTimeout  = flag.Duration("timeout", 5*time.Minute, "timeout for probes")
)

var (
	probeSuccess = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "log_collection_e2e_success",
		Help: "Probe success",
	})

	probeDuration = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "log_collection_e2e_duration_seconds",
		Help: "Probe duration (seconds)",
	})
)

var (
	sysLogger *log.Logger
	es        *elastic.Client
)

func init() {
	prometheus.MustRegister(
		probeSuccess,
		probeDuration,
	)
}

func getenv(k, dflt string) string {
	if s := os.Getenv(k); s != "" {
		return s
	}
	return dflt
}

func randomID() string {
	var b [16]byte
	if _, err := rand.Read(b[:]); err != nil {
		panic(err)
	}
	return "DYE" + hex.EncodeToString(b[:])
}

func buildQuery(id string, timestamp time.Time) elastic.Query {
	return elastic.NewBoolQuery().Must(
		elastic.NewMatchQuery("message", id),
		elastic.NewMatchQuery("_type", "_doc"),
		elastic.NewRangeQuery("@timestamp").From(timestamp.Add(-timeSkewAllowance)),
	)
}

func indexFromTimestamp(timestamp time.Time) string {
	return timestamp.Format(*indexFormat)
}

func isCanceled(err error) bool {
	return errors.Is(err, context.Canceled)
}

func runQuery(ctx context.Context, index string, query elastic.Query) (bool, error) {
	result, err := es.Search().Index(index).Query(query).From(0).Size(1).Do(ctx)
	if err != nil {
		return false, err
	}
	return result.TotalHits() > 0, nil
}

func runProbe(ctx context.Context) error {
	// Inject our dye marker into the log stream.
	id := randomID()
	timestamp := time.Now().UTC()
	sysLogger.Printf("%s", id)

	// Now wait until the message appears on ES. Do not call with
	// a Context lacking a deadline.
	index := indexFromTimestamp(timestamp)
	query := buildQuery(id, timestamp)
	deadline, _ := ctx.Deadline()
	for time.Now().Before(deadline) {
		time.Sleep(*pollInterval)
		ok, err := runQuery(ctx, index, query)
		if ok {
			return nil
		}
		if isCanceled(err) {
			return err
		}
		if err != nil {
			log.Printf("query error: %v", err)
		}
	}
	return errors.New("timed out without finding the dye marker")
}

func run(ctx context.Context) error {
	ticker := time.NewTicker(*probeInterval)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case t := <-ticker.C:
			// Create a context with a timeout.
			tctx, cancel := context.WithTimeout(ctx, *probeTimeout)
			err := runProbe(tctx)
			cancel()

			probeDuration.Set(time.Since(t).Seconds())
			switch {
			case err == nil:
				probeSuccess.Set(1)
			case isCanceled(err):
			default:
				log.Printf("probe error: %v", err)
				probeSuccess.Set(0)
			}
		}
	}
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	var err error
	sysLogger, err = syslog.NewLogger(syslogFacility|syslog.LOG_DEBUG, 0)
	if err != nil {
		log.Fatalf("can't open syslog logger: %v", err)
	}

	es, err = elastic.NewClient(elastic.SetURL(*elasticURL))
	if err != nil {
		log.Fatalf("error creating ES client: %v", err)
	}

	// Create a controlling context, and a signal handler that
	// will cancel it.
	outerCtx, cancel := context.WithCancel(context.Background())
	defer cancel()
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	// Spawn the HTTP listener and the probe runner in an
	// errgroup, whoever exits first will cause the program to
	// terminate cleanly.
	log.Printf("starting prober")
	eg, ctx := errgroup.WithContext(outerCtx)
	eg.Go(func() error {
		return run(ctx)
	})
	eg.Go(func() error {
		http.Handle("/metrics", promhttp.Handler())

		server := &http.Server{
			Addr: *addr,
		}
		go func() {
			<-ctx.Done()
			server.Close()
		}()
		return server.ListenAndServe()
	})
	if err := eg.Wait(); err != nil && err != http.ErrServerClosed && !isCanceled(err) {
		log.Fatalf("error: %v", err)
	}
}
