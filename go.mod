module git.autistici.org/ai3/tools/dye-injector

go 1.14

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/olivere/elastic/v7 v7.0.32
	github.com/prometheus/client_golang v1.10.0
	github.com/prometheus/common v0.20.0 // indirect
	golang.org/x/sync v0.8.0
)
